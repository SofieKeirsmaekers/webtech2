//navigator.geolocation.getCurrentPosition(success, error);

date = new Date();

var day = date.getDate();

var monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month = date.getMonth();

var dayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var weekday = date.getDay();

var fulldate = dayArray[weekday] + ", " + day + " " + monthArray[month];

console.log(fulldate);

var longitude;
var latitude;



var weatherDetails = function(details){
	var temp = Math.round((details.currently.temperature- 32)*(5/9));
			$('#temperatuur').text(temp + "°C");
			$('#weer').text(details.currently.summary);
			$('#icoon').attr('src', 'images/'+ details.currently.icon + '.png')
			$('#time').text(fulldate);
};



var App = function(){


	if(navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(showWeather);
	}
	else
	{
		alert("Geolocation is not supported by this browser.");
	}

	

	function showWeather(position){

		if (localStorage.getItem('temperature') !== null )
		{
			var details = JSON.parse(localStorage.getItem('temperature'));
			weatherDetails(details);
			
		}
		else{


			var url = "https://api.forecast.io/forecast/3eb6417c830fd7738d075ae30ce9fdb4/"+position.coords.latitude + "," + position.coords.longitude;

			$.ajax({
				type: 'GET',
				dataType: 'JSONP',
				url: url,
				success: function(resp) {
					var details = resp;

					localStorage.setItem("temperature", JSON.stringify(details));

					weatherDetails(details);

				},
				error: function() {

				}
			});

		};
	};

};
var myApp = new App();

