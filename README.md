# webtechnologie 2 - Sofie Keirsmaekers#

Tijdens de lessen Webtechnologie 2 kregen we dit jaar verschillende nieuwe 

## 1: Git to work ##
De eerste les leerde we met git werken. Deze tool helpt ons met groepswerken en het delen van projecten.

We leerden onder andere om branches aan te maken, changes te committen en deze daarna weer te pushen, en 2 branches te mergen. De meestgebruikte commands staan hieronder opgelijst.

	git pull: Haalt alle wijzigingen binnen.
	git branch (branchname): Een branch aanmaken.
	git checkout (branchname): Naar een branch gaan en deze aanmaken als ze niet bestaat.
	git add .: Voegt de gewijzigde bestanden toe om deze hierna te committen.
	git commit -m "tekst": Een change committen.
	git push: stuurt de aangepaste/nieuwe bestanden naar de repository.
	git merge (branchname): stuurt de bestanden van de gekozen branch naar de huidige branch.
	
	git status: toont alle wijzigingen en fouten met de repository.


### Opdracht ###
[Lab 1](https://github.com/SofieK/Lab1Webtech)Om git te leren kennen kregen we de opdracht om met vier een mini-website te bouwen. Ieder schreef 1 pagina, en doormiddel van pull, push, merge, .. moest dit één geheel worden in een git repository.
Om af te sluiten moesten we een pull request sturen met daarin de link van onze repository.


## 2:  CSS Animations##
De tweede les zagen we hoe we animaties konden maken door enkel CSS te schrijven. 

Om deze animaties te maken zijn er drie belangrijke acties te gebruiken: **Transities**, **animaties** en **transformaties**. 

####Transities####
Een transitie is een overgang tussen twee toestanden van een object. Deze overgang moet gestart worden door een :hover of een klasse toe te voegen/ te verwijderen via Javascript. Een transitie kan nooit beginnen zonder trigger.
	
	transition: property duration timing-function delay

**Property** Wat er moet veranderen (Een css eigenschap).

**Duration** Hoelang de overgang moet duren.

**timing-function** Hoe de overgang verloopt (Ease, cubic-brezier, ..).

**Delay** De tijd dat een transitie wacht om te beginnen, in plaats van onmiddelijk te starten.

Helaas vragen transities veel van onze browser, waardoor de prestaties slechter zijn. Hiervoor is wel een oplossing: Transformaties!

#### Transformaties ####

Met een transformatie is het mogelijk meerdere CSS eigenschappen tegelijk  te veranderen. Een transformatie kan 2D of 3D uitgevoerd worden.

##### 2D #####

	-webkit-transform: translate(20px, 20px) scale(2) rotate(45deg) skewX(15deg);

**translateX(x), translateY(y), translate(x, y)** Wijzigt de positie van het object op de X-as, de Y-as of de X- en de Y-as met de waarde die je tussen de haakjes meegeeft.

**scale(waarde)** Vergroot of verkleint het object met de waarde die meegegeven wordt.

**rotate(deg)** Draait het object rond de X-as.

**skewX(deg), skewY(deg), skew(deg)** Kantelt het object met de meegegeven waarde. Dit kan enkel langs de X- of Y-as, maar ook beiden.

##### 3D #####

	-webkit-transform: perspective(150) translate3d(40x, 30px, 20px) scale3d(2, 1.5, 0.5) rotate3d(3,4,1, 15deg);

**translate3d(x, y, z), translateZ(z), ..** Wijzigt de positie met de waarde die je tussen de haakjes meegeeft.

**perspective(waarde)** De waarde bepaald de afstand tot het object. Zo kan het perspectief op een 3d object veranderd worden. 

**scale3d(x,y,z), scaleZ(z), ..** Vergroot of verkleint het object met de waarde die meegegeven wordt.

**rotate3d(x, y, z), roateZ(z), ..** Kan het element ook rond de alle assen, de y-as of de z-as draaien.

#### Animaties ####

Om de code hierboven te combineren, gebruiken we animaties. Animaties kunnen in een ketting achter elkaar geplakt worden door gebruik te maken van keyframes.
Deze keyframes laten het toe om een animatie automatisch te laten starten, te loopen, ...

	animation: name duration timing-function delay iteration-count direction fill-mode play-state;

**name** De naam van de animatie. Deze naam wordt ook gebruikt om de keyframes in te stellen.

**duration** Hoe lang de animatie moet duren.

**timing-function** Het verloop van de animatie (ease, ease-out, ..).

**delay** De tijd dat een transitie wacht om te beginnen, in plaats van onmiddelijk te starten.

**iteration-count** Het aantal keer de animatie moet afspelen. Dit kan een getal zijn of 'infinite'.

**direction** De animatie in een bepaalde richting laten afspelen. Reverse om ze om te draaien en alternate om de animatie te laten wisselen tussen normaal en omgekeerd.

**fill-mode** Wat de animatie moet doen als ze afgelopen is: Forwards om alle eigenschappen te behouden, Backwards om terug te gaan naar de begintoestand van het object. Both combineerd Forwards en Backwards.


**play-state** De toestand van de animatie: Running of Paused


####Keyframes ####
	.div
	{
		display: block;
  		width: 100px;
  		height: 100px;
  		background-color: red;
  		-webkit-animation: animatie 2s ease-in infinite;
	}

	@-webkit-keyframes animatie
	{
		0%
		{}
		25%
		{-webkit-transform: translateY(100px);}
		50%
		{-webkit-transform: translateX(200px);}
		75%
		{-webkit-transform: translate(300px);}
		100%
		{}
	}

### Opdracht ###
[Lab 2](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/css-animations/?at=master) 
Om Css-animaties kregen we vier kleine oefeningen. Hierin moesten we zowel Transitions, tranformations als animations gebruiken. 

## 3:  Advanced Javascript##
We hadden al een basis van Javascript en we kenden al een framework zoals jQuery, maar door meer geavanceerde Javascript te leren, konden we onze eigen frameworks samenstellen. Hierdoor hebben we minder grote bestanden en zal de prestatie van onze website/app erop vooruit gaan.

Om zelf zoals Javascript te werken met de $ selector begonnen we alsvolgt:

	function WrapperElement(el)
    {
          this.element = el;
          this.isArray = el.length > 1 ? true : false;
    }

	
    WrapperElement.prototype.addClass = function(classname)
    {
    	this.element.className = classname;
	}
    
    function $(selector)
    {
    	var firstChar = selector.substring(0,1);
        switch(firstChar)
        {
            case "#":
            	var select = selector.substring(1, selector.length);
            	return new WrapperElement(document.getElementById(select));
            	break;

        }
	}

Hierop bouwden we verder dat deze code ook voor . of gewone elementen zou werken.
Daarna maakten we via hetzelfde principe ook de functies zoals .css, ... 

### Opdracht ###
[Lab 3](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/advanced-js/?at=master)
De opdracht bij Advanced Javascript was om een todo-app te bouwen met Javascript. De beginbestanden kregen we, we moesten zelf enkel een mini framework schrijven om de app te doen werken.

## 4:  API##

Tijdens de les over API's zijn we begonnen met JSON. JSON is een manier om Javascript objecten beter te schrijven:

	[{
		'firstName: "Niels",
		'lastName: "Roels"
	},
	{
		'firstName: "Jos",
		'lastName: "Vermeulen"
	}]

Om APIs te gebruiken hadden we een ajax-call nodig om de data binnen te halen:

	$.ajax{
		type: 'GET',
		url:'[link json-file]',
		dataType: 'jsonp',
		succes: function(resp){
			localstorage('[naam voor localstorage]', JSON.stringify(resp));
		},
		error: function(){
			console.log('Fail');
		}
	};

De localstorage zorgt ervoor dat de data niet telkens opnieuw uit de API moet worden gehaald. Dit zou enorm veel prestaties vragen van zowel de browser als de API. De localstorage slaat de data tijdelijk op.

Om de data van een bepaalde locatie te gebruiken, hebben we de geolocator gebruikt: 

	navigator.geolocation.getCurrentPosition(succes, error);

Hierdoor gaat de browser opzoek naar de huidige locatie nadat deze toestemming heeft gevraagd aan de gebruiker. 

### Opdracht ###
[Lab 4](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/API/?at=master)
Met de API forecast.io moesten we een weerapp maken die het huidige weer kon weergeven en live kon updaten. Hiervoor kregen we enkel de oefening van tijdens de les. 

## 5:  Node.js en Websockets##

Node.js is een javascript platform met een ingebouwde node-server.
Node wordt in Javascript geschreven en vormt een alternatief voor PHP.

Node werkt met de Node Package Manager (NPM) om modules te installeren. Deze modules maken applicaties in node eenvoudiger om te schrijven. Modules kunnen worden opgeslagen in een package.json file. Deze file onthoud welke modules er gebruikt zijn en maakt het eenvoudig om deze tegelijk te installeren.

	npm install -g 'module' : globaal op de computer installeren
	npm install --save 'module : installeert de module in de applicatie en saved in de package.json
	npm install : installeerd alle modules vermeld in de package.json

We werkten met een Node framework genaamd Express.
Dit framework moet ook geinstalleerd worden met NPM (best globaal), en kan daarna de startbestanden van een project klaarzetten.

Om een project te testen moet node een bepaald bestand runnen. 
Voor Express:

	node bin/www


#### Websockets ####
Socket.io is een websocket dat live updates kan sturen tussen verschillende gebruikers en schermen. Deze updates worden doorgevoerd zonder te refreshen. 

#### MongoDB ####
Tijdens deze les hebben we ook met MongoDB gewerkt. Dit is een databank systeem dat zonder relaties werkt. Het is eenvoudig in gebruik, maar heeft minder mogelijkheden dan een MySQL-Database.



### Opdracht ###
[Lab 5a](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/Nodejs_Mongodb/?at=master)
De gastsprekers gaven ons de startbestanden om een frigo-app te maken. Deze moest live updaten aan alle gebruikers wanneer er iets wijzigde.

[Lab 5b](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/NodeApp2/?at=master)
<br>Een aantal lessen later kregen we hier een tweede opdracht voor om Node.js en Websockets nog wat te verduidelijken. Deze keer was de opdracht om een Q&A app te  bouwen.

## 6:  Angular.js##

Angular is een Javascript Framework dat het maken van Single Page Applications vereenvoudigd. 

Angular werkt met code in de HTML en in een .js file. In de HTML kan Angular herkend worden aan de 'ng-'.

Een aantal attributen opgelijst:

	ng-app="": Vanaf hier wordt er gecontroleerd op Angular code in de HTML en hiermee geven we ook een naam aan de app.
	ng-model="": Een model waarin we info kunnen opslaan. Als we dit op een input zetten, slaan we de ingetypte tekst op.
	ng-if="": Toont enkel de volgende code wanneer er aan de if voldaan is.
	ng-repeat="": Werkt als een for-loop. Wordt uitgevoerd voor elk element dat aan de voorwaarde voldoet.
	ng-click="": voert iets uit wanneer er geklikt wordt.

	{{ waarde }} Hiermee kunnen we waarden laten afdrukken die door een ng-model of via de javascript worden bepaald.
	
De Javascript code heeft ook zijn eigen attributen:

	var app = angular.module('IMD', []);
	-> hangt vast met ng-app
	
	app.controller('myController', function($scope){});
	-> In een controller schrijven we alle functies om onze applicatie te laten werken.
	-> $scope: maakt het ons mogelijk om de controller en de view met elkaar te laten communiceren


	

### Opdracht ###
[Lab 6](https://bitbucket.org/SofieKeirsmaekers/webtech2/src/5bf9500ecaafaf5ac260bb8cb8ff060e0481adf4/Angular/?at=master)
De opdracht voor Angular kwam ook van een gastspreker en was een applicatie om drank te bestellen. Er moesten drankjes kunnen toegevoegd worden, gefilterd worden, het aantal van een drankje wijzigen en de bestelling als laatste doorsturen.

## 7:  SASS + BEM ##

#### BEM ####
BEM helpt ons onze html te structureren zodat het schrijven van CSS eenvoudiger wordt. Door deze methodologie toe te passen, wordt de code ook meer leesbaar.

	.block
	.block__element
	.block--modifier
	.block__element--modifier

#### SASS ####
SASS wordt geschreven in SCSS-files, en opgeslagen in een aparte SCSS-folder. Daarnaast bestaat er nog steeds de standaard CSS-file, deze moet dezelfde naam hebben als de SCSS-file.
In het SCSS mapje zit nog een submap genaamd modules, waarin we onze modules opslaan.

#### SASS naar CSS ####
Via de terminal kunnen we SASS omzetten naar CSS. Dit kan manueel en automatisch.
Daarnaast kunnen we de CSS-file ook compressen om de prestaties van onze website te verbeteren.
	
	sass [locatie van SCSS-file]:[locatie van CSS-file]  -  manueel 
	sass --watch [locatie van SCSS-file]:[locatie van CSS-file]  -  automatisch
	sass --watch style.scss:../css/style.css --style compressed  -  compressen


#### Variabelen en nesting ####
Als we kleuren willen hergebruiken, kunnen we deze in een variabele steken en telkens deze variabele oproepen. Dit maakt het eenvoudiger om een kleur aan te passen omdat je enkel de variabele moet veranderen in plaats van 20 eigenschappen.

	$color1: #626262;
	
	section
	{
		background-color: $color1;
	}

Wanneer we de span in een h1 in een div willen aanspreken, wordt dit ook eenvoudiger door nesting. 

	[SASS]
	.divke
	{
		background-color: #222
		
		h1
		{
			text-decoration: underline;
			
			span
			{
				font-weight: bold;
			}
		}
	}

####Mixins####

Een mixin is een kleine functie die we kunnen gebruiken in css. Voorbeelden zijn het uitrekenen van een lettertype of het bereken van een donkerdere tint achtergrond, ...

	@mixin functie-naam($v)
	{
		code
	}

Een mixin kunnen we in onze SASS-code oproepen door:

	h1
	{
		@include functie-naam(waarde);
	}

Deze mixins zijn online makkelijk te verkrijgen door frameworks zoals bourbon. Het gebruik van een framework kan heel wat tijd uitsparen tijdens het schrijven van SASS.

## 8:  GULPJS ##

Gulp is een tool die het werken een pak eenvoudiger maakt. Met gulp kunnen we een heleboel vermoeiende taken automatiseren om ze sneller af te handelen. Bijvoorbeeld, CSS minify-en of foto's optimaliseren.

Gulp werkt met tasks die we in de gulpfile.js moeten schrijven. Een task zullen we zelden zelf moeten schrijven. Er bestaat een hele library met tasks in de gulp documentatie. Om een task aan te maken hebben we deze code nodig:

	gulp.task('Naam van de task', function(){
		"Code uit de library"
	});


Zo kunnen we een waslijst aan tasks maken. Daarna kunnen we al deze tasks combineren in 1 commando:

	gulp.task('default', ['Task1, Task2, Task3]);

Als we al deze tasks automatisch willen laten uitvoeren gaan we naar de commandline en typpen we: 
	
	gulp


Om slechts één task uit te voeren:

	gulp 'naam van de task'
